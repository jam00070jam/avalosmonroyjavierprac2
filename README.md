Práctica a Realizar

Vamos a simular el funcionamiento de un sistema de ascensores en un edificio compuesto por diferentes plantas. Para ello será utilizarán los siguientes elementos:

● Un monitor para el control de ascensores: Será el encargado de centralizar las operaciones que deben realizar los procesos que representarán a las diferentes personas y los diferentes ascensores para simular la operatividad real. Para ello se diseñará con los siguientes procedimientos:

○ esperarAscensor()_: Permitirá a un proceso persona esperar a un ascensor antes de  subirse  al  mismo.  Hay  que  utilizar  estructuras  diferenciadas  para  solicitar  la  dirección  del  ascensor,  esto es, **SUBIDA** o  **BAJADA**.

○ ascensorEnPlanta()_:  Simulará  la  llegada  de  un  ascensor  a  una  planta  del  edificio.  Primero deberán bajarse las personas que quieren ir a esa planta antes de permitir la entrada de  nuevas personas en  la dirección del ascensor.

○ subidoAscensor()_:  Simulara  el  tiempo  que  una  persona  se  encuentra  subida  en  el  ascensor  antes  de bajarse en la planta de  destino.
● ProcesoPersona(id): Este proceso simulará el comportamiento de una persona que quiere entrar en el edificio y moverse por las diferentes plantas del edificio.

○ Las  personas  siempre  llegan  al  edificio  por  la  planta  baja  del  mismo.

○Generarán  una  planta  aleatoria  a  la  que  quieren  ir.

○ Una vez que llegan a la planta de destino simularán por un tiempo de hasta 30 segundos  que  deben permanecer  en esa planta.
○Se repetirá el proceso de la generación de una nueva planta hasta un NUM_OPERACIONES como máximo, es decir, si no ha abandonado el edificio antes, lo abandonará en esa última generación de planta, para esa última generación la planta elegida será la planta baja.

○ En cuanto la persona seleccione la planta baja, y baje del ascensor en ella, abandonará el edificio.

○ Deberá presentar por pantalla los pisos en los que espera, indicando el destino, y cuando llegue al destino deberá indicar el tiempo que estará ocupado en esa planta.

ProcesoAscensor(id): Este proceso simulará el comportamiento del ascensor, es decir, se moverá entre las plantas recogiendo a las personas que esperan y dejándolas en la planta de su destino:

○ El proceso comienza su ejecución en la planta baja.

○Para simular su operación realizará los siguientes pasos:

■ Una vez que tenga personas que se dirigen a una planta en su dirección se moverá planta a planta.

■ En cada planta comprobará si hay personas que quieren bajarse y luego permitirá la subida de personas hasta que se complete el ascensor (CAPACIDAD_ASCENSOR).

■ El ascensor en una dirección llegará hasta la última planta en esa dirección antes de cambiar el sentido de la marcha.

○Una vez que el ascensor se ha puesto en marcha ya no parará. Completará una dirección antes de cambiar el sentido.

○ Cada vez que un ascensor llegue a una planta se deberá indicar por la pantalla.

● Hilo principal: Permitirá hacer una prueba de simulación para el diseño del monitor y los procesos de personas y ascensor:

○Creará una instancia del monitor con un NUM_ASCENSORES y un NUM_PLANTAS. Esto permitirá simular el edificio.

○ e crearán los procesos que representan a los ascensores y se lanzarán para su ejecución.

○ Se creará un proceso persona en un tiempo variable de hasta 10 segundos. Una vez que se hayan generado un número de procesos NUM_PERSONAS ya no se generarán más.

○ Esperará hasta que todas las personas abandonen el edificio antes de terminar su ejecución.

Solución

Se presenta la solución de la práctica dividida en las dos partes quue se pedía, primero el análisis y posteriormente el diseño.

1.Análisis

Se describirá las estructuras de datos, variables compartidas y procedimientos necesarios para comprender el diseño que se realiza de la práctica.

Datos

Tipos de datos necesarios para la solución de la práctica:

● TDA Persona

o idPersona_: entero.

o plantaOrigen_: entero

o plantaDestino_: entero.

o contadorOperaciones_: entero

o dirección:_ entero.

o Operaciones:

	■  tiempoDestino(). Tiempo que pasa una persona en una planta.

	■  generarDestino(). Crea una planta como destino.

	■  generarDireccion(). Genera hacia donde se dirige la persona.
● <TDA Ascensor

o idAscensor_: entero.

o dirección_: entero.

o plantaActual_: entero.

o Operaciones:

	■  siguientePlanta(). Calcula cual va a ser la siguiente planta.
● Hilo Principal

o generaMonitor(). Genera el monitor.

o generaAscensor(). Genera los ascensores.

o generaPersonas().  Genera las personas.

o esperaFin().  Espera que terminen todos los procesos.

o finalizaAscensor().  Finaliza los ascensores.

o finalizaHiloPrincipal(). Finaliza los procesos del hilo principal.
● TDA Monitor

o ascensor[NUM_ASCENSOR][NUM_PLANTA]:

o personaEnAscensor[NUM_PERSONAS]:

o direccionAscensor[NUM_ASCENSOR]:

o pedirAscensor[NUM_ASCENSOR]:

o ascensorDestino[NUM_ASCENSOR]
Procedimientos apoyo

● esperarAscensor(plantaOrigen,dirección)_. Permitirá a un proceso persona esperar a un ascensor antes de subirse al mismo. Hay que utilizar estructuras diferenciadas para solicitar la dirección del ascensor, esto es, SUBIDA o BAJADA.

● ascensorEnPlanta(idAscensor,dirección,plantaActual)_. Simulará la llegada de un ascensor a una planta del edificio. Primero deberán bajarse las personas que quieren ir a esa planta antes de permitir la entrada de nuevas personas en la dirección del ascensor

● subidoAscensor(idAscensor,plantaOrigen,plantaDestino,dirección)._ Simulara el tiempo que una persona se encuentra subida en el ascensor antes de bajarse en la planta de destino.

2.Diseño

Se presenta el diseño de la práctica mediante pseudocódigo donde se resolverá la ejecución necesaria para cada uno de los procesos implicados.

● Hilo Principal

Variables locales:

ascensor :  Ascensor

persona : Persona

monitorControlAscensor: Monitor

monitorControlAscensor = generaMonitor(NUM_ASCENSOR,NUM_PLANTA) //se genera un monitor para el control de personas y ascensores

generaAscensor(monitorControlAscensor)

for (int i=0;i < NUM_ASCENSORES;i++){ 	//se generan los ascensores

	ascensor = generaAscensor(i,monitorControlAscensor)
	generaAscensor(ascensor)

}

for (int i =0; i<NUM_PERSONAS;i++){  //se generan las personas

	persona= generaPersona(i, monitorControlAscensor)
	generaPersona(persona)

}

esperaFIN() // se espera el fin de las ejecuciones del monitor
● Proceso Persona

Variables locales:

plantaOrigen : entero

plantaDestino: entero

dirección:  entero

idPersona:  entero

contadorOperaciones:  entero
Ejecución (){

plantaOrigen=BAJO //Nos dice que la planta origen es BAJO

contadorOperaciones=1

plantaDestino=generarDestino() //Al hacer el primero destino el contador sube a 1

do{

	if(contadorOperaciones=NUM_OPERACIONES) //Vemos si caben operaciones

		plantaOrigen=BAJO //entra una nueva persona

	dirección=generarDireccion(plantaOrigen,plantaDestino);

	ascensor=monitor.esperarAscensor(dirección,plantaOrigen)

	monitor.subidoAscensor(ascensor,dirección,plantaOrigen, plantaDestino)

	tiempoDestino()

	plantaOrigen=plantaDestino

	plantaDestino=generarDestino()

	contadorOperaciones++

}while(plantaOrigen!=BAJO) //Hasta que salga la persona
}

tiempoDestino(){//Se genera un tiempo aleatorio hasta que la persona vuela a coger el ascensor

MAX_TIEMPO=30

t_destino=aleatorio(MAX_TIEMPO)

ocupado(t_destino)
}

generarDestino(){//Se genera el destino

do{

	destino=aletorio(NUM_PLANTAS)

}while(destino==plantaOrigen)//No se puede tener la plantaOrigen como destino

return destino;
}

generarDireccion(plantaOrigen,plantaDestino){

if(plantaDestino>plantaOrigen)

	return SUBIDA

else

	return BAJADA
}

Proceso Ascensor

Variables locales:

idAscensor: entero

dirección: entero

plantaActual:  entero
Ejecución() { //simula la acción del ascensor

plantaActual=BAJO

direccion=SUBIDA

while(direccion != SUBIDA || direccion != BAJADA){

	monitor.ascensorEnPlanta(idAscensor,plantaActual,dirección)

	plantaActual=siguientePlanta(dirección)

	if (plantaActual==NUM_PLANTAS)

		direccion=BAJADA

	if( plantaActual==BAJO)

		direccion=SUBIDA

}
}

siguientePlanta(idAscensor,direccion){

if (direccion==SUBIDA)

	return plantaActual+1

if (direccion==BAJADA)

	return plantaActual-1
● Monitor

Variables locales:

personaEnAscensor[idAscensor]: entero

direccionAscensor[idAscensor]: entero

ascensorDestino[idAscensor]: entero

pedirAscensor[direccion][plantaActual] : condición

ascensor[idAscensor][plantaActual] : condición

ascensorParado[idAscensor] :  condición
Control monitores:

esperarAscensor(direccion,plantaOrigen){ //

delay.pedirAscensor[direccion][plantaActual)//bloquea para que no se puedan pedir el ascensor

return ascensorDestino //devuelve donde se debe subir la persona
}

ascensorEnPlanta(plantaActual,direccion){

direccionAscensor[idAscensor]=direccion

if (NOT EMPTY(ascensor[idAscensor][planta])){//comprobamos si hay personas para subir o bajar

	resume.ascensor[idAscensor][planta]

	delay.ascensorParado[idAscensor]

}else

if( personaEnAscensor[idAscensor] <  CAPACIDAD_ASCENSOR) && (NOT (pedirAscensor[direccion][plantaActual) //se quiere subir gente y hay hueco

	ascensorDestino = idAscensor

	resume.pedirAscensor[direccion][plantaActual]

	delay.ascensorParado[idAscensor]
}

subidoAscensor(idAscensor,direccion,plantaOrigen,plantaDestino){

personaEnAscensor[idAscensor]++ //se sube una persona al ascensor

if( personaEnAscensor[idAscensor <CAPACIDAD_ASCENSOR) && (NOT (pedirAscensor[direccion][plantaOrigen}//se quiere subir otra persona

	resume.pedirAscensor[direccion][plantaOrigen]

	delay.ascensor[idAscensor][plantaDestino]

else //no caben mas personas o no hay nadie para subirse

	resume.ascensorParado[idAscensor]

delay.ascensor[idAScensor][plantaDestino]

personaEnAscensor[idAscensor]-—// se baja la persona

if(NOT EMPTY(ascensor[idAscensor][plantaDestino]

	resume.ascensor[idAscensor][plantaDestino]

else

	if(NOT(pedirAscensor[direccionAscensor[idAscensor]] [PlantaDestino]) //hay personas que van el la direccion del ascensor

		ascensorDestino = idAscensor

		resume.pedirAscensor[direccionAscensor[idAscensor]][PlantaDestino])

	else

		resume.ascensorParado[idAscensor]//se libera el ascensor para que prosiga
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.introduccion.avalosmonroyjavierprac2;

import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac2.HiloPrincipal.BAJADA;
import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac2.HiloPrincipal.BAJO;
import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac2.HiloPrincipal.NUM_PLANTAS;
import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac2.HiloPrincipal.SUBIDA;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Usuario
 */
public class Persona implements Runnable {

    private final static int NUM_OPERACIONES = 3;
    private final static int MAX_TIEMPO = 5;
    private final int idPersona;
    private final MonitorControlAscensor monitor;
    private int plantaOrigen;
    private int plantaDestino;
    private int contadorOperaciones;
    private int direccion;
    private int ascensor;
    private final Semaphore personaSale;

    public Persona(int idPersona, MonitorControlAscensor monitor, Semaphore personaSale) {
        this.idPersona = idPersona;
        this.monitor = monitor;
        this.personaSale = personaSale;
    }

    private int generarDestino() {//Se genera el destino
        int destino;
        do {
            destino = ThreadLocalRandom.current().nextInt(NUM_PLANTAS);
        } while (destino == plantaOrigen);//No se puede tener la plantaOrigen como destino
        return destino;
    }

    private int generarDireccion(int plantaOrigen, int plantaDestino) {//Se genera la direccion
        if (plantaDestino > plantaOrigen) {
            return SUBIDA;
        } else {
            return BAJADA;
        }
    }

    private void tiempoDestino() throws InterruptedException {//Se genera un tiempo aleatorio hasta que la persona vuela a coger el ascensor
        int tiempoEnSegundos = ThreadLocalRandom.current().nextInt(MAX_TIEMPO) + 1;
        System.out.println("La persona (" + idPersona + ") estará en la planta " + plantaDestino + " durante " + tiempoEnSegundos + " segundos");
        TimeUnit.SECONDS.sleep(tiempoEnSegundos);
    }

    @Override
    public void run() {
        System.out.println("La persona (" + idPersona + ") ha entrado en el edificio");
        try {
            personaSale.acquire();
            plantaOrigen = BAJO;//Nos dice que la planta origen es BAJO
            plantaDestino=generarDestino();
            contadorOperaciones = 1;
            do {
                if (contadorOperaciones == NUM_OPERACIONES) //Vemos si caben operaciones
                    plantaOrigen = BAJO; //entra una nueva persona
		
                direccion = generarDireccion(plantaOrigen, plantaDestino);
                ascensor = monitor.esperarAscensor(plantaOrigen,direccion);
                monitor.subidoAscensor(ascensor, direccion, plantaOrigen, plantaDestino);
                tiempoDestino();
                plantaOrigen = plantaDestino;
                plantaDestino = generarDestino();
                contadorOperaciones++;
            } while (plantaOrigen != BAJO);//Hasta que salga la persona
            System.out.println("La persona (" + idPersona + ") ha salido");
            personaSale.release();
        } catch (InterruptedException e) {
            System.out.println("Se ha interrumpido a la persona (" + idPersona + ")");
        }
    }
}
    
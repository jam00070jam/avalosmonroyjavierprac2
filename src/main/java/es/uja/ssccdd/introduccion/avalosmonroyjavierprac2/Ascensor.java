/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.introduccion.avalosmonroyjavierprac2;

import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac2.HiloPrincipal.BAJADA;
import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac2.HiloPrincipal.BAJO;
import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac2.HiloPrincipal.NUM_PLANTAS;
import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac2.HiloPrincipal.SUBIDA;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Usuario
 */
public class Ascensor implements Runnable {

    private static final int TIEMPO_PLANTA = 500;
    private final int idAscensor;
    private final MonitorControlAscensor monitor;
    private int direccion;
    private int plantaActual;

    public Ascensor(int idAscensor, MonitorControlAscensor monitor) {
        this.idAscensor = idAscensor;
        this.monitor = monitor;
    }
    private int siguientePlanta(int direccion) throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(TIEMPO_PLANTA);
        if (direccion==SUBIDA)
            return plantaActual+1;
        else
            return plantaActual-1;  
    }

    @Override
    public void run() {
        System.out.println("El ascensor (" + idAscensor + ")comiena a ejecutarse");
        plantaActual=BAJO;
	direccion=SUBIDA;
        try{
            while(direccion != SUBIDA || direccion != BAJADA){//Bucle infinito ya que la direccion es siempre de SUBIDA o BAJADA
                System.out.println("Ascensor (" + idAscensor + ") llega a la planta " + plantaActual);
                monitor.ascensorEnPlanta(idAscensor,plantaActual,direccion);
                plantaActual=siguientePlanta(direccion);	
                if (plantaActual==NUM_PLANTAS)
                        direccion=BAJADA;
                if( plantaActual==BAJO)
                        direccion=SUBIDA;
            }
        }catch (InterruptedException e){
            System.out.println("El ascensor ("+ idAscensor +") interrumpido");
        }
    }

}

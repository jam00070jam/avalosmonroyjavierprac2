/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.introduccion.avalosmonroyjavierprac2;

import static es.uja.ssccdd.introduccion.avalosmonroyjavierprac2.HiloPrincipal.CAPACIDAD_ASCENSOR;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Usuario
 */
public class MonitorControlAscensor {
    private final int numAscensores;
    private final int numPlantas;
    private int ascensorDestino;
    private final int personaEnAscensor[];
    private final int direccionAscensor[];
    private final ReentrantLock lock;
    private final Condition ascensorParado[];  //Condición
    private final Condition pedirAscensor[][]; //Condición
    private final Condition ascensor[][];      //Condición
    private final int pedirAscensorContador[][]; //Contador de bloqueos
    private final int ascensorContador[][];         //Contador de bloqueos

    public MonitorControlAscensor(int numAscensores, int numPlantas) {
        this.numAscensores = numAscensores;
        this.numPlantas = numPlantas;
        personaEnAscensor = new int[numAscensores];
        direccionAscensor = new int[numAscensores];
        ascensorDestino = 0;
        lock= new ReentrantLock();
         
        ascensorParado = new Condition[this.numAscensores];        
                
        int i, j;
        for (i = 0; i < this.numAscensores; i++) {
            ascensorParado[i] = lock.newCondition();
        }
        pedirAscensor = new Condition[2][this.numPlantas];
        pedirAscensorContador = new int[2][this.numPlantas];
        for (i = 0; i < 2; i++) {
            for (j = 0; j < this.numPlantas; j++) {
                pedirAscensor[i][j] = lock.newCondition();
                pedirAscensorContador[i][j] = 0;
            }
        }
        ascensor = new Condition[this.numAscensores][this.numPlantas];
        ascensorContador = new int[this.numAscensores][this.numPlantas];
        for (i = 0; i < this.numAscensores; i++) {
            for (j = 0; j < this.numPlantas; j++) {
                ascensor[i][j] = lock.newCondition();
                ascensorContador[i][j] = 0;
            }
        }
    }
    public int esperarAscensor(int plantaOrigen, int direccion) throws InterruptedException{
        lock.lock();
        try {
            pedirAscensorContador[direccion][plantaOrigen]++;
            pedirAscensor[direccion][plantaOrigen].await();  //Esperamos el ascensor
            return ascensorDestino;     //devuelve donde se debe subir la persona
        }finally {
            lock.unlock();
        }
    }
    public void subidoAscensor(int idAscensor, int plantaOrigen, int plantaDestino, int direccion) throws InterruptedException{
        lock.lock();
        try{
            personaEnAscensor[idAscensor]++;//se sube una persona al ascensor
            if((personaEnAscensor[idAscensor] < CAPACIDAD_ASCENSOR) && 
               (pedirAscensorContador[direccion][plantaOrigen]> 0)){//se quiere subir otra persona
                pedirAscensorContador[direccion][plantaOrigen]--;
                pedirAscensor[direccion][plantaOrigen].signal();
                ascensorContador[idAscensor][plantaDestino]++;
                ascensor[idAscensor][plantaDestino].await();
            }else{//no caben mas personas o no hay nadie para subirse
                ascensorParado[idAscensor].signal();
                ascensorContador[idAscensor][plantaDestino]++;
                ascensor[idAscensor][plantaDestino].await();
            }
            personaEnAscensor[idAscensor]--;// se baja la persona
            if (ascensorContador[idAscensor][plantaDestino]>0){
                ascensorContador[idAscensor][plantaDestino]--;
                ascensor[idAscensor][plantaDestino].signal();
            }else{//hay personas que van el la direccion del ascensor
                if(pedirAscensorContador[direccionAscensor[idAscensor]][plantaDestino]>0){
                    ascensorDestino=idAscensor;
                    pedirAscensorContador[direccionAscensor[idAscensor]][plantaDestino]--;
                    pedirAscensor[direccionAscensor[idAscensor]][plantaDestino].signal();
                }else
                    ascensorParado[idAscensor].signal();//se libera el ascensor para que prosiga
            }
                    
        }finally{
            lock.unlock();
        }        
    }
    public void ascensorEnPlanta(int idAscensor, int direccion, int planta) throws InterruptedException{
        lock.lock();
        try{
            direccionAscensor[idAscensor]=direccion;
            if(ascensorContador[idAscensor][planta]>0) {//comprobamos si hay personas para subir o bajar
               ascensorContador[idAscensor][planta]--;
               ascensor[idAscensor][planta].signal();
               ascensorParado[idAscensor].await();
            }else {
                if ( (personaEnAscensor[idAscensor] < CAPACIDAD_ASCENSOR)//se quiere subir gente y hay hueco
                    && (pedirAscensorContador[direccion][planta]>0)) {   
                        ascensorDestino = idAscensor;
                        pedirAscensor[direccion][planta].signal();
                        pedirAscensorContador[direccion][planta]--;
                        ascensorParado[idAscensor].await();
                }
            }   
        }finally{
            lock.unlock();
        }
    }
}
    

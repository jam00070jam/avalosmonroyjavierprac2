/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.introduccion.avalosmonroyjavierprac2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Usuario
 */
public class HiloPrincipal {
    public static final int BAJO = 0;
    public static final int SUBIDA = 0;
    public static final int BAJADA = 1;
    public static final int NUM_ASCENSORES = 2;
    public static final int NUM_PLANTAS = 3;
    public static final int NUM_PERSONAS = 4;
    public static final int CAPACIDAD_ASCENSOR = 2;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)  throws InterruptedException {
        // TODO code application logic here
        System.out.println("Inicio del hilo principal");
        ExecutorService marco = Executors.newFixedThreadPool(NUM_ASCENSORES+NUM_PERSONAS);  //Creeamos el marco de ejecución
        MonitorControlAscensor monitor =  new MonitorControlAscensor(NUM_ASCENSORES, NUM_PLANTAS);  //Se genera un monitor para el control de personas y ascensores
        Semaphore esperarPersonas = new Semaphore(NUM_PERSONAS);
        
        int i;
        for ( i = 0; i < NUM_ASCENSORES; i++){    //Creo y ejecuto los ascensores              
            Ascensor nuevoAscensor = new Ascensor(i,monitor);
            marco.submit(nuevoAscensor);
            TimeUnit.SECONDS.sleep(1);
        }
        for ( i = 0; i < NUM_PERSONAS; i++){      //creo y ejecuto las personas
            Persona nuevaPersona = new Persona(i, monitor, esperarPersonas);
            marco.submit(nuevaPersona);
            TimeUnit.SECONDS.sleep(1);
        }
        esperarPersonas.acquire(NUM_PERSONAS);   //esperamos que las personas finalicen
        
        marco.shutdownNow();                                       
        marco.awaitTermination(1, TimeUnit.DAYS);

        System.out.println("Fin del hilo principal");
    }
    
}
